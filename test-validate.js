const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=>{
  context('Function isUserNameValid', ()=> {
     it('Function prototype : boolean isUserNameValid(username: String)', ()=> {
         expect(validate.isUserNameValid('tar')).to.be.true;
     });

     it('less than 3 character', ()=>{ 
        expect(validate.isUserNameValid('ta')).to.be.false;
     });
     it('all character lower-case', ()=>{
        expect(validate.isUserNameValid('Tar')).to.be.false;
        expect(validate.isUserNameValid('taR')).to.be.false;   
     });
     it('more than 15 character', ()=>{
         expect(validate.isUserNameValid('tar123456789012')).to.be.true;
         expect(validate.isUserNameValid('tar1234567890123')).to.be.false;
     });
  });

  context('Function isAgeValid', ()=> {
     it('Function prototype : boolean isAgeValid (age: String)', ()=>{
        expect(validate.isAgeValid('18')).to.be.true;
     });
     it('age is only number', ()=>{
         expect(validate.isAgeValid('a')).to.be.false;
     });
     it('age not less than 18 and not more than 100', ()=> {
        expect(validate.isAgeValid('17')).to.be.false;
        expect(validate.isAgeValid('18')).to.be.true;
        expect(validate.isAgeValid('100')).to.be.true;
        expect(validate.isAgeValid('101')).to.be.false;
     });
  });
});