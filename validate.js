
module.exports = {
    isUserNameValid: function(username){
        if(username.length < 3 || username.length >15 ) {
            return false;
        }
        if(username.toLowerCase() !== username) {
            return false;
        }

        return true;
    }, 
    isAgeValid: function(age){
        if( Integer(age) == age ) {
            return false;
        }
        return true;
    }
}